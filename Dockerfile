# Usando uma imagem base nginx
FROM nginx:alpine

# Copiando os arquivos da página web para o diretório padrão do Nginx
COPY ./PrimeClean /usr/share/nginx/html

# Expondo a porta 80, que é a porta padrão do Nginx
EXPOSE 80

# Comando para iniciar o servidor web quando o contêiner for executado
CMD ["nginx", "-g", "daemon off;"]